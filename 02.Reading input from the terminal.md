# Reading input from the terminal

## Overview

- Very useful to ask the user to enter data into a program via the terminal/console
- The C library contains several input functions, and scanf() is the most general of them can read a variety of formats
- reads the input from the standard input stream stdin and scans that input according to the format provided
format can be a simple constant string, but you can specify %s, %d, %c, %f, etc., to read strings,
integer, character or floats
- If the stdin is input from the keyboard then text is read in because the keys generate text characters: letters digits, and punctuation
  - when you enter the integer 2014, you type the characters 2 0 1 and 4
  - you want to store that as a numerical value rather than as a string, your program has to convert the string character-by-character to a numerical value and this is the job of the scanf function
- scanf() returns an integer value that tells you how many items were successfully read and assigned
  - if you ask scanf() to read an integer and the user enters a string, then scanf() will read the first character of the string and stop

## Scanf

- like printf(), scanf() uses a control string followed by a list of arguments control string indicates the destination data types for the input stream of characters
- the printf() function uses variable names, constants, and expressions as its argument list the scanf()function uses pointers to variables
- Remember these 3 rules about scanf
  - returns the number of items that it successfully reads
  - If you use scanf() to read a value for one of the basic variable types we've discussed, precede the variable name with an &
  - If you use scanf() to read a string into a character array, don't use an &.
- The scanf() function uses whitespace (newlines, tabs, and spaces) to decide how to divide the input into separate fields
- scanf is the inverse of printf(), which converts integers, floating-point numbers, characters, and C strings to text that is to be displayed onscreen

```c
int main () {
    char str[100];
    int i;

    printf("Enter a value ");
    scanf("%s %d", str, &i);

    printf("\nYou entered: %s %d", str, i);

    return 0;
}
```

![alt text](img/image1.png)
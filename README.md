# C

## Topics

- Basic Operators - logical, arithmetic and assignment
- Conditional Statements -making decisions(if, switch)
- Repeating code -looping(for, while, and do-while)
- Arrays - defining and initializing, multi-dimensional
- Functions - declaration and use, arguments and parameters, call by value vs. call by reference
- Debugging - call stack, common mistakes, understanding compiler messages
- Structs - initialing, nested structures, variants
- Character Strings - basics, array of chars, character operations
- Pointers = definition and use, using with functions and arrays, malloc, pointer arithmetic
- The Preprocessor - #define, #include, #ifdef, #endif
- Input and Output -getchar, scanf
- File Input/Output - reading and writing to a file, file operations
- Standard C Library - string functions, math functions, utility functions, standard header files

## Overview

- C is a general-purpose, imperative computer programming language supports structured programming
  - Uses statements that change a program' state, focuses on how
- Currently used in many operating systems, embedded systems, and applications
- C is a modern language
  - has most basic control structures, data types, and functions
  - designed for top-down planning
  - organized around the use of functions and data structures
  - a very reliable and readable program
- C is used on everything from minicomputers, Unix/Linux systems to pc's and mainframes
- C is the preferred language for producing word processing programs, spreadsheets and compilers
- C has become popular for programming embedded systems
  - used to program microprocessors found in automobiles, cameras, DVD players, etc
- C has and continues to play a strong role in the development of Linux
- C programs are easy to modify and easy to adapt to new models or languages
- In the 1990s, many software houses began turning to the C++ language for large programming projects
- C is subset of C++ with object-oriented programming tools added
  - any C program is a valid C++ program
  - By learning C, you also learn much of C++
- C remains a core skill needed by corporations and ranks in the top 10 of desired skills 
- C provides constructs that map efficiently to typical machine instructions and thus is used by programs that were previously implement in assembly language
  - provides low-level access to memory(has many low-level capabilities)
  - requires minimal run-time support(fast execution)
- C initially became widely known as the development language of the UNIX operating system
  - virtually all new major operating systems are written in C and/or C++
- C evolved from a previous language called BCPL
  - uses many of the important features of BCPL, such as pointers and arrays
  - C is a superset of BCPL, meaning that all valid BCPL programs are valid C programs

## Efficiency and Portability

- C is an efficient language
  - takes advantage Of the capabilities Of current computers
  - C programs a re compact and fast (similar tO assembly language programs)
  - programmers can fine-tune their programs for maximum speed or most efficient use Of memory
- C is a portable language
  - C programs written on one system can be run 0 n other systems with little or no modification
  - a leader in portability
  - compilers are available for many computer architectures
- Linux/Unix systems typically come with a C compiler as part of the package
  - compilers are available for personal computers

## Power and Flexibility

- the Unix/Linux kernel is written in C
- many compilers and interpreters for other languages (FORTRAN, Perl, Python, Pascal, LISP, Logo, and BASIC)have been written in C
- C programs have been used for solving physics and engineering problems and even for animating special effects
for movies
- C is flexible
  - used for developing just about everything you can imagine by way of a computer program
  - accounting applications to word processing and from games to operating systems
  - it is the basis for more advanced languages, such as C++ 
- It is also used for developing mobile phone apps in the form of Objective C
- C is easy to learn because of its compactness
  - is an ideal first language to learn if you want to be a programmer
  - you will acquire sufficient knowledge for practical application development quickly and easily

## Programmer Oriented

- C fulfills the needs of programmers
  - gives you access to hardware
  - enables you to manipulate individual bits in memory
- C contains a large selection of operators which allows you to express yourself succinctly
- C is less strict than most languages in limiting what you can do
  - Can be both an advantage and a danger
    - advantage is that many tasks (such as converting forms of data) are easier in C
    - danger is that you can make mistakes (pointers) that are impossible in some languages
    - C gives you more freedom, but it also puts more responsibility on you
- C implementations have a large library of useful C functions
  - deal with common needs of most programmers

![alt text](img/image.png)
#include <stdio.h>
#include <stdlib.h>

void multiplyTwoNumbers(int a, int b) {
    printf("The product of %d and %d is %d\n", a, b, a * b);
}

void main() {
    multiplyTwoNumbers(5, 10);
    multiplyTwoNumbers(7, 3);
    multiplyTwoNumbers(2, 8);
}
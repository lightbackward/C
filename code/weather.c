#include <stdio.h>
#include <stdlib.h>
#define MONTHS 12
#define YEARS 5

int main() {
  float arr[YEARS][MONTHS] = {
      // first year
      {1.2, 1.6, 2.1, 3.6, 5.6, 7.2, 8.7, 9.3, 8.0, 6.1, 4.2, 2.4},
      // second year
      {0.8, 1.1, 1.5, 2.6, 4.8, 6.0, 7.3, 8.1, 7.1, 5.1, 3.2, 1.6},
      // third year
      {0.2, 0.6, 1.1, 2.3, 4.1, 5.9, 6.8, 7.9, 6.9, 5.0, 3.1, 1.5},
      // fourth year
      {0.1, 0.3, 0.7, 1.5, 2.6, 4.3, 5.7, 6.2, 5.6, 4.3, 2.7, 1.3},
      // fifth year
      {0.3, 0.6, 1.0, 2.2, 3.9, 5.5, 6.8, 7.3, 6.7, 5.0, 3.2, 1.6}};

  for (int i = 0; i < YEARS; i++) {
    float sum = 0;
    for (int j = 0; j < MONTHS; j++) {
      sum += arr[i][j];
    }
    printf("The total temperature for year %d is %.2f\n", i + 1, sum);
    printf("The average temperature for year %d is %.2f\n", i + 1, sum / 12);
  }

  return 0;
}
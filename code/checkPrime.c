#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#define MAX_LENGTH 50

int main() {
  int p;
  int i;

  int primes[MAX_LENGTH] = {0}; // 定义一个长度为MAX_LENGTH的数组，用于存储素数
  int primeIndex = 2; // 定义一个变量，用于记录素数的个数

  bool isPrime; // = true;

  // hardcode the first two primes
  primes[0] = 2;  
  primes[1] = 3;

  // 从5开始，每次加2，判断是否为素数
  for (p = 5; p < MAX_LENGTH; p += 2) {
    isPrime = true;
    // 遍历已知的素数，判断p是否能被素数整除
    for (i = 0; i < primeIndex; i++) {
      if (p % primes[i] == 0) {
        isPrime = false;
        break;
      }
    }

    // 如果p是素数，则将其存入数组，并更新素数的个数
    if (isPrime) {
      primes[primeIndex] = p;
      primeIndex++;
    }
  }

  for (i = 0; i < primeIndex; ++i) {
    printf("%d ", primes[i]);
  }

  return 0;
}

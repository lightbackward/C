#include <stdio.h>

int main() {
  char str1[] = "To be or not to be";
  char str2[] = ", this is the question";
  unsigned int count = 0;

  while(str1[count] != '\0') {
    ++count;
  }

  printf("Length of str1 is %d\n", count);

  count = 0;

  while(str2[count] != '\0') {
    ++count;
  }

  printf("Length of str2 is %d\n", count);

  return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// int main () {
//     printf("Hello World!\n");
//     return 0;
// }

// int main () {
//     char str[100];
//     int i;

//     printf("Enter a value ");
//     scanf("%s %d", str, &i);

//     printf("\nYou entered: %s %d", str, i);

//     return 0;
// }

// int main() {
//     int integerVar = 10;
//     float floatingVar = 33.99;
//     double doubleVar = 20.22;

//     _Bool boolVar = 0;

//     return 0;
// }

// int main() {
//     enum gender
//     {
//         male,
//         female
//     };

//     enum gender myGender;

//     myGender = male;

//     enum gender anotherGender = female;

//     bool isMale = (myGender == male);

//     return 0;
// }

// int main (void){
//     int integerVar = 100;
//     double floatingVar = 331.79;
//     double doubleVar = 8.44e+11;
//     int charVar = 'W';
//     bool boolVar = 0;

//     printf("integerVar = %i\n", integerVar);
//     printf("floatingVar = %f\n", floatingVar);
//     printf("doubleVar = %e\n", doubleVar);
//     printf("doubleVar = %g\n", doubleVar);
//     printf("charVar = %c\n", charVar);
//     printf("boolVar = %i\n", boolVar);

//     return 0;
// }

// int main(int argc, char *argv[]) {
//     int numberOfArguments = argc;
//     printf("Number of arguments: %d\n", numberOfArguments);

//     char *argument1 = argv[0];
//     printf("First argument: %s\n", argument1);

//     char *argument2 = argv[1];
//     printf("Second argument: %s\n", argument2);

//     return 0;
// }

// int main(int argc, char *argv[]) {
//     double width = atof(argv[1]);
//     double height = atof(argv[2]);

//     printf("Width: %f\n", width);
//     printf("Height: %f\n", height);

//     double area = width * height;
//     double perimeter = (width * 2) + (height * 2);

//     printf("Area: %f\n", area);
//     printf("Perimeter: %f\n", perimeter);

//     return 0;
// }


// int main() {
//     enum Company {
//         Google,
//         Microsoft = 9,
//         Apple
//     };

//     enum Company myCompany = Apple;

//     printf("Apple = %d\n", myCompany);

//     return 0;
// }

// int main() {
//     int i = 0;

//     while(i < 10) {
//         printf("i = %d\n", i);
//         i++;
//     }
// }

// int main() {
//     unsigned int a = 60;
//     unsigned int b = 13;
//     int result = 0;

//     unsigned c = a & b;
//     printf("a & b = %d\n", c);
// }

// int main() {
//     int x = 0;
//     float f = 12.98;
//     x = f;

//     printf("x = %d\n", x);
// }

// int main() {
//     double minutesEntered = 0;
//     double days = 0;
//     double minutesInYear = 0;

//     printf("Enter the number of minutes: ");
//     scanf("%lf", &minutesEntered);

//     days = minutesEntered / 1440;
//     minutesInYear = (minutesEntered / 1440) / 365;

//     printf("Minutes entered: %lf\n", minutesEntered);
//     printf("Days: %lf\n", days);
//     printf("Minutes in a year: %lf\n", minutesInYear);

//     return 0;
// }

// void main() {
//     int Number = 25;
//     int *pNumber = &Number;

//     printf("Address of Number: %p\n", pNumber);
//     printf("Value of Number: %d\n", *pNumber);
// }

// void main() {
//     char text[] = "Every dog has his day.";
//     char word[] = "dog";
//     char *pFound = NULL;
//     pFound = strstr(text, word);

//     printf("Found: %s\n", pFound); // Found: dog has his day.
// }

// This program demonstrates the use of the strtok function to split a string into tokens based on a specified delimiter.
// It prints each token on a new line.

// // Entry point of the program
// void main() {
//     char str[80] = "Hello, how are you - my name is - John";
//     const char s[2] = "-";
//     char *token;

//     // Tokenize the string using the specified delimiter
//     token = strtok(str, s);

//     // Continue to extract tokens until no more tokens are found
//     while(token!= NULL) {
//         printf("%s\n", token);
//         token = strtok(NULL, s);
//     }
// }

// int main() {
//     char buff[100];
//     int nLetters = 0;
//     int nDigits = 0;
//     int nPunct = 0;

//     printf("Enter a string of less than %lld characters: \n", sizeof(buff) - 1);
//     if (fgets(buff, sizeof(buff), stdin) != NULL) {
//         // TODO: Add code to count letters, digits, and punctuation here.
//     }

//     return 0;
// }


int main() {
    // int number = 99;
    // void *pNumber = &number;

    // printf("Address of number: %p\n", pNumber);
    // printf("Value of number: %d\n", *(int *)pNumber);

    // return 0;

    // int count = 10, x;
    // int *int_pointer = &x;

    // int_pointer = &count;
    // x = *int_pointer;

    // *int_pointer += 1;

    // printf("count = %i, x = %i, int_pointer = %i\n", count, x, *int_pointer);
    // return 0;

    // int arraySum(int array[], const int n);
    // int values[10] = {3, 7, 1, 9, 5, 2, 8, 4, 6, 0};

    // printf("The sum of the array is: %d\n", arraySum(values, 10));

    // return 0;

    // char multiple[] = "Hello, world!";
    // char *p = multiple;

    // for (int i = 0; i < strnlen(multiple, sizeof(multiple)); i++){
    //     printf("multiple[%d] = %c *(p+%d) = %c &multiple[%d] = %p p+%d = %p\n", i, multiple[i], i, *(p+i), i, &multiple[i], i, p+i);
    // }

    // char *str;

    // str = (char *)malloc(15);
    // strcpy(str, "Hello, world!");

    // printf("String = %s, Address = %u\n", str, str);

    // str = (char *)realloc(str, 20);
    // strcat(str, " How are you?");
    // printf("String = %s, Address = %u\n", str, str);

    // free(str);

    // return 0;

    // int *ptr;

    // int value = 88549;

    // ptr = (int *)malloc(sizeof(value));
    // printf("Address of value: %p\n", ptr);
    // *ptr = value;

    // for (; ptr!= NULL; ptr++) {
    //     printf("Value of value: %d\n", *ptr);
    //     *ptr = *ptr + 1;
    // }

    //     printf("Value of value: %d\n", *ptr);
    // free(ptr);
    // return 0;

    FILE *fp;
    int c;

    fp = fopen("./input.txt", "r");

    if (fp == NULL){
        perror("Error opening file");
        return 1;
    }

    while((c = fgetc(fp)) != EOF)
        printf("%c", c);

    fclose(fp);
    fp = NULL;

    system("pause");

    return 0;
}   

int arraySum(int arr[], const int n) {
    int sum = 0, *ptr;
    int * const arr_end = arr + n;

    for (ptr = arr; ptr < arr_end; ptr++) {
        sum += *ptr;
    }

    return sum;
}